package net.pervukhin.bpmlightfront.managedbeans;

import net.pervukhin.bpmlightfront.dto.Activity;
import net.pervukhin.bpmlightfront.dto.Deployment;
import net.pervukhin.bpmlightfront.dto.LifecycleType;
import net.pervukhin.bpmlightfront.dto.PaginationContainerDTO;
import net.pervukhin.bpmlightfront.dto.Process;
import net.pervukhin.bpmlightfront.dto.Variable;
import net.pervukhin.bpmlightfront.service.ActivityService;
import net.pervukhin.bpmlightfront.service.DeploymentService;
import net.pervukhin.bpmlightfront.service.ProcessService;
import net.pervukhin.bpmlightfront.service.UtilService;
import net.pervukhin.bpmlightfront.service.VariableService;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ViewScoped
@ManagedBean
public class ProcessManagedBean {
    @Autowired
    private ProcessService processService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private DeploymentService deploymentService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private VariableService variableService;

    private Process selectedInstance;

    private Integer selectedTab;
    private String flowInProgress;
    private String flowCompleted;

    private Boolean isActive;
    private ProcessModel processes;
    private ActivityModel activities;
    private VariableModel variables;

    private Deployment currentDeployment;

    public ProcessManagedBean(@Autowired ProcessService processService) {
        this.processService = processService;
        this.isActive = Boolean.parseBoolean(
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("isActive"));
        this.processes = new ProcessModel(isActive);
    }

    public ProcessModel getProcesses() {
        return processes;
    }

    public ActivityModel getActivities() {
        return activities;
    }

    public VariableModel getVariables() {
        return variables;
    }

    public String getDateString(Date date) {
        return utilService.getDateAsString(date);
    }

    public String getRunTime(Date startDate, Date endDate) {
        return utilService.getRunTime(startDate, endDate);
    }

    public void selectInstance(Process process) {
        this.currentDeployment = deploymentService.getById(process.getProcessDefinitionId());
        this.activities = new ActivityModel(process.getProcessInstanceId());
        this.variables = new VariableModel(process.getProcessInstanceId());
        final List<String> inProgressList = new ArrayList<>();
        final List<String> completedList = new ArrayList<>();
        activityService.getList(0, 100, process.getProcessInstanceId()).getList()
                .forEach(item -> {
                    if (LifecycleType.STARTED.equals(item.getLifecycleType())) {
                        inProgressList.add(item.getActivityId());
                    }
                    if (LifecycleType.ENDED.equals(item.getLifecycleType())) {
                        completedList.add(item.getActivityId());
                    }
                });
        this.flowCompleted = completedList.stream().collect(Collectors.joining( "," ));
        this.flowInProgress = inProgressList.stream().collect(Collectors.joining( "," ));
    }

    public Process getSelectedInstance() {
        return selectedInstance;
    }

    public void setSelectedInstance(Process selectedInstance) {
        this.selectedInstance = selectedInstance;
    }

    public Integer getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(Integer selectedTab) {
        this.selectedTab = selectedTab;
    }

    public String getFlowInProgress() {
        return flowInProgress;
    }

    public String getFlowCompleted() {
        return flowCompleted;
    }

    public String getXml() {
        return this.currentDeployment == null ? "" : this.currentDeployment.getXml();
    }

    class ProcessModel extends LazyDataModel<Process> {
        private Boolean isActive;

        public ProcessModel(Boolean isActive) {
            this.isActive = isActive;
        }

        @Override
        public List<Process> load(int first, int pageSize, Map<String, SortMeta> sort, Map<String, FilterMeta> filter) {
            final PaginationContainerDTO<Process> result = processService.getList(first, pageSize, isActive);
            this.setRowCount(result.getCount());
            return result.getList();
        }
    }

    class ActivityModel extends LazyDataModel<Activity> {
        private String processInstanceId;

        public ActivityModel(String processInstanceId) {
            this.processInstanceId = processInstanceId;
        }

        @Override
        public List<Activity> load(int first, int pageSize, Map<String, SortMeta> sort, Map<String, FilterMeta> filter) {
            final PaginationContainerDTO<Activity> result = activityService.getList(first, pageSize, processInstanceId);
            this.setRowCount(result.getCount());
            return result.getList();
        }
    }

    class VariableModel extends LazyDataModel<Variable> {
        private String processInstanceId;

        public VariableModel(String processInstanceId) {
            this.processInstanceId = processInstanceId;
        }

        @Override
        public List<Variable> load(int first, int pageSize, Map<String, SortMeta> sort, Map<String, FilterMeta> filter) {
            final PaginationContainerDTO<Variable> result = variableService.getList(first, pageSize, processInstanceId);
            this.setRowCount(result.getCount());
            return result.getList();
        }
    }
}
